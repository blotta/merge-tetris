extends Node2D

export (int) var width;
export (int) var height;
export (int) var x_start;
export (int) var y_start;
export (int) var offset;

signal yellow_reached_top
signal yellow_row_deleted(count)

onready var all_blocks : Array = []

onready var StepTimer = $StepTimer

var packed_blocks = {
	"blue" : preload("res://Scenes/BlueBlock.tscn"),
	"red" : preload("res://Scenes/RedBlock.tscn"),
	"green" : preload("res://Scenes/GreenBlock.tscn"),
	"yellow" : preload("res://Scenes/YellowBlock.tscn")
}

var blue_ref = null
var blue_block = null
var move_down = false
var row_deleted := false
var _grid_paused := false

var yllw_block_grid : Array

var desired_move : Vector2
var desired_rot : int

func _ready():
	randomize()
	StepTimer.wait_time = 0.7
	StepTimer.start()
	yllw_block_grid = make_2d_array(width, height, null)

func _process(delta):
	if _grid_paused: return
	
	if blue_block != null:
#		desired_move := Vector2.ZERO
#		desired_rot : int = 0;
		if Input.is_action_just_pressed("ui_right"):
			desired_move = Vector2(1, 0)
			#blue_block.move(Vector2(1, 0))
		elif Input.is_action_just_pressed("ui_left"):
			desired_move = Vector2(-1, 0)
			#blue_block.move(Vector2(-1, 0))
		elif Input.is_action_just_pressed("ui_up"):
			desired_rot = -90
		elif Input.is_action_just_pressed("ui_down"):
			desired_rot = 90
		elif Input.is_action_just_pressed("push_down"):
			desired_move = Vector2(0, -1)

func _physics_process(delta):
	# move down any yellow
	if row_deleted and blue_ref.get_ref() == null:
		for row in range(1, height):
			move_yllw_row_down(row)
		row_deleted = false
	
	# Detect if yellow reached top
	for col in range(width):
		if yllw_block_grid[col][height - 1] != null:
			emit_signal("yellow_reached_top")
	
	# Spawn new blue and reds
	if blue_block == null:
		gen_falling_blocks()
	
	if blue_block != null:
		if desired_move != Vector2.ZERO:
			var can_move = true
			
			# check greens
			for grn_p in blue_block.connected_blocks:
				if is_something_blocking(grn_p, desired_move):
					can_move = false
			# check blue
			if is_something_blocking(blue_block, desired_move):
				can_move = false
			
			# move blue
			if can_move:
				blue_block.move(desired_move)
			
			desired_move = Vector2.ZERO
		elif desired_rot != 0:
			var can_rotate = true
			for grn_p in blue_block.connected_blocks:
				#print("Green piece offset of ", grn_p.blue_offset," rotated ",
				#	desired_rot, "deg = " ,grn_p.rotated_offset(desired_rot))
				var possible_pos = pixel_to_grid(blue_block.position) + grn_p.rotated_offset(desired_rot)
				if not is_valid_grid_pos(possible_pos):
					can_rotate = false
					break
				for b in all_blocks:
					if b.color != "green" and pixel_to_grid(b.position) == possible_pos:
						can_rotate = false
						break
			
			if can_rotate:
				#blue_block.rot = desired_rot
				for grn_p in blue_block.connected_blocks:
					grn_p.blue_offset = grn_p.rotated_offset(desired_rot)
			
			desired_rot = 0
	else:
		print("Blue null")
	
	# move blue and red down
	if move_down:
		for b in all_blocks:
			if b.color == "blue" or b.color == "red":
				if not is_something_blocking(b, Vector2(0, -1)):
					b.move(Vector2(0, -1))
		move_down = false
	
	# move greens
	if blue_block != null:
		for green_p in blue_block.connected_blocks:
			#print(pixel_to_grid(blue_block.position) + green_p.blue_offset)
			green_p.place(pixel_to_grid(blue_block.position) + green_p.blue_offset)

	
	# check if any reached the bottom or yellow
	var blue_or_grn_reached_bottom_or_yllw = false
	for b in all_blocks:
		var below = b.whats_to_the(Vector2(0, -1));
		if below == null:
			continue
		if (below is Block and below.color == "yellow") or pixel_to_grid(b.position).y == 0:
			if b.color == "red":
				destroy_block(b)
			elif b.color == "blue" or b.color == "green":
				blue_or_grn_reached_bottom_or_yllw = true
	
	if blue_block != null and blue_or_grn_reached_bottom_or_yllw:
		# freeze shape
		for grn_p in blue_block.connected_blocks:
			#blue_block.connected_blocks.remove(blue_block.connected_blocks.find(grn_p))
			change_block_color(grn_p, "yellow")
		change_block_color(blue_block, "yellow")
	
	# check if row is full of yellow, delete it and move other yellow down
	var full_yllw_rows_idxs := []
	for row in range(0, height):
		if is_yellow_row_full(row):
			full_yllw_rows_idxs.append(row)
	for row in full_yllw_rows_idxs:
		delete_yllw_row(row)
		row_deleted = true
	if full_yllw_rows_idxs.size() > 0:
		emit_signal("yellow_row_deleted", full_yllw_rows_idxs.size())

func pause_grid(yn : bool):
	_grid_paused = yn
	$StepTimer.set_paused(_grid_paused)

func is_grid_paused() -> bool:
	return _grid_paused

func move_yllw_row_down(start_row : int):
	var row := start_row
	var yllw_mask : Array
	yllw_mask.resize(width)
	
	while row > 0:
		for col in range(0, width):
			yllw_mask[col] = 0
		
		for col in range(0, width):
			var ylw_b = yllw_block_grid[col][row]
			if ylw_b != null:
				yllw_mask[col] = 1
				var new_gpos = pixel_to_grid(ylw_b.position)
				new_gpos.y -= 1
				if new_gpos.y < 0: return
				if yllw_block_grid[new_gpos.x][new_gpos.y] != null: return
#				if is_something_blocking(ylw_b, Vector2(0, -1)):
#					return
		
		if not yllw_mask.has(1):
			# empty row
			return
		# didn't return, so nothing blocking
		#delete_yllw_row(row)
		#row -= 1
#		for col in range(0, width):
#			if yllw_mask[col] != 0:
#				spawn_block_at(Vector2(col, row), "yellow")
		for col in range(0, width):
			move_yllw_block_down(yllw_block_grid[col][row])
		row -= 1

func move_yllw_block_down(b):
	if b == null: return
	var gpos = pixel_to_grid(b.position)
	var new_gpos = gpos
	new_gpos.y -= 1
	yllw_block_grid[gpos.x][gpos.y] = null
	#b.place(new_gpos)
	b.move(Vector2(0, -1))
	yllw_block_grid[new_gpos.x][new_gpos.y] = b


func delete_yllw_row(row):
	for col in range(width):
		var ylw_b = yllw_block_grid[col][row]
		if ylw_b != null:
			destroy_block(ylw_b)

func is_yellow_row_full(row : int) -> bool:
	var yllw_count = 0
	for col in range(width):
		if yllw_block_grid[col][row] != null:
			yllw_count += 1
	if yllw_count == width:
		return true
	return false

func is_something_blocking(block, dir : Vector2) -> bool:
	#print("Checking path for ", block.name, " going ", dir)
	var something_blocking = false
	var path_obj = block.whats_to_the(dir)
	#print("path_obj is ", path_obj)
	if (path_obj is Block):
		#print("path_obj is Block")
		if path_obj.color != "green" and path_obj.color != "blue":
			something_blocking = true
	elif path_obj is Area2D:
		#print("path_obj is Area2D")
		# Border
		something_blocking = true
	#print("Something blocking: ", something_blocking, " what: ", block.name,": ", path_obj)
	return something_blocking

func gen_falling_blocks():
	for pos in gen_col_pos(4):
		var current_spawn_color = "red"
		if blue_block == null:
			current_spawn_color = "blue"
		var new_block = spawn_falling_block(pos, current_spawn_color)

func spawn_falling_block(col : int, color : String):
	var b = spawn_block_at(Vector2(col, height - (randi() % 2 + 1)), color);
	return b;

func spawn_block_at(gpos : Vector2, color : String):
	var b = packed_blocks[color].instance()
	add_child(b)
	all_blocks.append(b)
	b.grid = self
	b.place(gpos)
	match b.color:
		"blue":
			blue_block = b
			blue_ref = weakref(b)
		"red":
			b.connect("blue_green_touched", self, "_on_bg_touch_red")
		"green":
			blue_block.connected_blocks.append(b)
			#print("Added green block: ", blue_block.connected_blocks)
			b.blue_offset = gpos - pixel_to_grid(blue_block.position)
		"yellow":
			yllw_block_grid[gpos.x][gpos.y] = b
	return b

func change_block_color(block, color):
	var gpos = pixel_to_grid(block.position)
	destroy_block(block)
	spawn_block_at(gpos, color)

func destroy_block(b : Node2D):
	all_blocks.remove(all_blocks.find(b))
	match b.color:
		"blue":
			blue_block = null
			pass
		"red":
			b.disconnect("blue_green_touched", self, "_on_bg_touch_red")
		"yellow":
			var gp := pixel_to_grid(b.position)
			yllw_block_grid[gp.x][gp.y] = null
	b.queue_free()

func make_2d_array(w, h, value) -> Array:
	var array = []
	for i in w:
		array.append([])
		for j in h:
			array[i].append(value)
	return array

func gen_col_pos(count: int) -> Array:
	var col_pos : Array = [];
	var pos : int;
	for i in count:
		pos = randi() % width;
		while col_pos.has(pos):
			pos = randi() % width;
		col_pos.append(pos);
	return col_pos;

func grid_to_pixel(gpos : Vector2) -> Vector2:
	var new_x = x_start + offset * gpos.x;
	var new_y = y_start + -offset * gpos.y;
	return Vector2(new_x, new_y);

func pixel_to_grid(pos : Vector2) -> Vector2:
	var col : int = floor(float((pos.x - 32))/offset);
	var row : int = height - 1 - floor(float((pos.y - 192))/offset);
	#print("px: ", x, " -> ", col, " ; py: ", y, " -> ", row);
	return Vector2(col, row);

func is_valid_grid_pos(pos : Vector2) -> bool:
	#print("checking pos ", pos);
	if pos.x > width - 1 or pos.x < 0:
		return false;
	if pos.y > height - 1 or pos.y < 0:
		return false;
	return true;

func _on_StepTimer_timeout():
	move_down = true

func _on_bg_touch_red(red_b):
	#print("Red touched by blue or green")
	change_block_color(red_b, "green")
