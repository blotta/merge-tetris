extends Node2D

enum GameState {
	MENU = 5,
	GRID
}

var state setget set_state,get_state
func set_state(value):
	if state != value:
		var old_state = state
		print("setting state from ", state, " to ", value)
		state = value
		print("state set to ", value)
		_state_changed(old_state, state)

func get_state():
	return state

var score : int
var game_over : bool

var packed_grid2 = preload("res://Scenes/grid2.tscn")
var packed_ui = preload("res://Scenes/UI.tscn")
var packed_menu = preload("res://Scenes/MainMenu.tscn")
var packed_game_over_panel = preload("res://Scenes/GameOverPanel.tscn")
var packed_pause_panel = preload("res://Scenes/PausePanel.tscn")

onready var grid2 : Node2D
onready var ui : Control
onready var menu : Control
onready var game_over_pnl : Control
onready var pause_pnl : Control

func _ready():
	set_state(GameState.MENU)

func _process(delta):
	if Input.is_action_just_pressed("pause"):
		if grid2 != null and game_over == false:
			if !grid2.is_grid_paused():
				grid2.pause_grid(true)
				add_pause_pnl()
			else:
				grid2.pause_grid(false)
				remove_pause_pnl()

func _state_changed(old_state, new_state):
	print("Changing state from ", old_state, " to ", new_state)
	
	remove_game_over_pnl()
	remove_pause_pnl()
	
	if old_state == GameState.MENU:
		print("Removing Menu")
		remove_menu()
	elif old_state == GameState.GRID:
		remove_game()
	
	if new_state == GameState.MENU:
		print("Invokinging Menu")
		invoke_menu()
	elif new_state == GameState.GRID:
		new_game()

func invoke_menu():
	menu = packed_menu.instance()
	add_child(menu)
	menu.get_node("PlayButton").connect("pressed", self, "_on_play_btn_pressed")
	menu.get_node("QuitButton").connect("pressed", self, "quit_game")

func remove_menu():
	if menu != null:
		menu.get_node("PlayButton").disconnect("pressed", self, "_on_play_btn_pressed")
		menu.get_node("QuitButton").disconnect("pressed", self, "quit_game")
		menu.queue_free()
		menu = null

func new_game():
	score = 0
	game_over = false
	grid2 = packed_grid2.instance()
	add_child(grid2)
	grid2.connect("yellow_reached_top", self, "_on_yellow_reached_top")
	grid2.connect("yellow_row_deleted", self, "_on_yellow_row_deleted")
	
	ui = packed_ui.instance()
	add_child(ui)
	ui.get_node("ScorePanel/Label").text = "Score: 0"

func remove_game():
	if grid2 != null:
		grid2.disconnect("yellow_reached_top", self, "_on_yellow_reached_top")
		grid2.disconnect("yellow_row_deleted", self, "_on_yellow_row_deleted")
		grid2.queue_free()
		grid2 = null
	if ui != null:
		ui.queue_free()
		ui = null

func game_over():
	grid2.disconnect("yellow_reached_top", self, "_on_yellow_reached_top")
	game_over = true
	add_game_over_pnl()
	grid2.pause_grid(true)
	print("GAME OVER")

func add_game_over_pnl():
	game_over_pnl = packed_game_over_panel.instance()
	add_child(game_over_pnl)
	game_over_pnl.get_node("ScoreLabel").text = "Final Score: %d" % score
	game_over_pnl.get_node("QuitButton").connect("pressed", self, "_on_gop_quit")

func remove_game_over_pnl():
	if game_over_pnl != null:
		game_over_pnl.get_node("QuitButton").disconnect("pressed", self, "_on_gop_quit")
		game_over_pnl.queue_free()
		game_over_pnl = null

func add_pause_pnl():
	pause_pnl = packed_pause_panel.instance()
	add_child(pause_pnl)
	pause_pnl.get_node("QuitButton").connect("pressed", self, "_on_gop_quit")

func remove_pause_pnl():
	if pause_pnl != null:
		pause_pnl.get_node("QuitButton").connect("pressed", self, "_on_gop_quit")
		pause_pnl.queue_free()
		pause_pnl = null

func quit_game():
	get_tree().quit()

func _on_gop_quit():
	set_state(GameState.MENU)

func _on_play_btn_pressed():
	set_state(GameState.GRID)

func _on_yellow_row_deleted(count):
	score += 10 * count
	ui.get_node("ScorePanel/Label").text = "Score: %d" % score

func _on_yellow_reached_top():
	game_over()
