extends Block
class_name RedBlock

signal blue_green_touched(red_block)

func _ready():
	pass

func _on_RedBlock_area_entered(area):
	var owner = area.owner
	if owner is Block and (owner.color == "blue" or owner.color == "green"):
		emit_signal("blue_green_touched", self)
