extends Block
class_name GreenBlock

var blue_offset : Vector2

func move(dir):
	pass

func rotated_offset(deg : int):
	var rot_off : Vector2 = blue_offset
	rot_off = blue_offset.rotated(deg2rad(float(deg)))
	rot_off.x = round(rot_off.x)
	rot_off.y = round(rot_off.y)	
	return rot_off

func _ready():
	pass
