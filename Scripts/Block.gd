extends Area2D
class_name Block

export (String) var color : String

var grid

var neighbors = {
	Vector2(-1, 0) : "Left",
	Vector2(1, 0) : "Right",
	Vector2(0, 1) : "Up",
	Vector2(0, -1) : "Down"
}

const MOVE_AMOUNT = 64

func _ready():
	#print("Block spawned")
	pass

func whats_to_the(dir : Vector2):
	var areas : Array = get_node(neighbors[dir]).get_overlapping_areas()
	if areas.size() > 0:
		return areas[0]
	else:
		return null

func move(dir : Vector2):
	#if whats_to_the(dir) == null:
	position += dir * Vector2(1, -1) * MOVE_AMOUNT

func place(gpos : Vector2):
	position = grid.grid_to_pixel(gpos)
