# Merge Tetris

Godot Wild Jam #13

## Theme

Merge

## Gameplay

The player controls the blue block, being able to move left and right and rotate clockwise and counter-clockwise.

Red blocks will continually be spawned from the top at random quantities across the width of the grid.

When there isn't a blue block in game, the next spawned block will be blue, otherwise, red.

Red blocks turn green when a blue or green block touches it, meaning it is now merged to the falling shape, thus following its movement and rotation relative to the blue block.

When red blocks reach the bottom, they simply disappear. When blue and green blocks reach the bottom, all merged blocks become yellow, meaning they cannot be moved further.

When one or more rows in the grid are completely filled with yellow blocks the rows are cleared, granting points to the player depending on the number of rows cleared at once. After being cleared, other yellow rows on top go down until one of the blocks reaches bottom or another yellow block.

There is no win condition. Only a score counter.

If a yellow block reaches the top, the player loses the game.